# #######################################################################################################################################################

#

#                            ###   Welcome to the Biomarin's Data Camp Statistics Course in R!  ###

# This course is designed to provide you with a solid understanding of statistical concepts and how to apply them in the R programming language.

# In this introductory course we will explore:

# -descriptive statistics,

# -measures of central tendency and variability,

# -graphical techniques for displaying and summarizing data,

# -how to run a simple linear regression model

#

# Towards the end of the course you can take a simple test to pass and get a certificate

#

# Author: Jai Singh (jai.singh@bmrn.com)

# Date: 4/14/2023

#

# #######################################################################################################################################################

## For this class we will be using the popular iris dataset.

## Data Set Information:

## This is perhaps the best known database to be found in the pattern recognition literature.

## The data set contains 3 classes of 50 instances each, where each class refers to a type of iris plant. One class is linearly separable from the other 2;

## the latter are NOT linearly separable from each other.

## Predicted attribute: class of iris plant.

## Attribute Information:

# 1. sepal length in cm

# 2. sepal width in cm

# 3. petal length in cm

# 4. petal width in cm

# 5. class:

# -- Iris Setosa

# -- Iris Versicolour

# -- Iris Virginica

# #######################################################################################################################################################



## Descriptive statistics

# Load built-in iris dataset using the data() command

data(iris)



# Get a structure of all the columns in the . This shows that the data has 5 columns, 4 of which are numbers and one is a factor

str(iris)



# Get a summary of all the columns in the dataset.The summary shows the mean, and 25% and 75% values in each column (i.e. the quartiles)

summary(iris)



# Now lets calculate mean, median, and standard deviation of the iris dataset column sepal.length. These are all measures of central tendency and deviation.

mean(iris$Sepal.Length)

median(iris$Sepal.Length)

sd(iris$Sepal.Length)



# Create a histogram of Sepal Lengths

hist(iris$Sepal.Length)



# boxplot of Petal Length

boxplot(iris$Sepal.Length)



# boxplots of four columns of iris.

boxplot(iris[, 1:4])



# Now lets use the dplyr package to get the mean and standard deviation in sepal.length grouped by the different Species.

library(dplyr)

iris %>%
  
  group_by(Species) %>%
  
  summarise(sepal_length_mean = mean(Sepal.Length),
            
            sepal_length_sd = sd(Sepal.Length)
            
  )





# Inferential statistics

# Load built-in mtcars dataset

data(mtcars)



# Perform a t-test on mpg for cars with 4 and 6 cylinders

t.test(mpg ~ cyl, data = mtcars, subset = cyl %in% c(4,6))



# Create a scatter plot of horsepower and weight

plot(mtcars$hp, mtcars$wt)



# Fit a linear regression model to predict mpg based on horsepower

model <- lm(mpg ~ hp, data = mtcars)

summary(model)



# Create a plot of the regression line and data points

plot(mtcars$hp, mtcars$mpg)

abline(model)





# Hypothesis Testing

# One-sample t-test

t.test(data$variable, mu=0)



# Two-sample t-test

t.test(data$variable ~ data$group)



# Chi-square test

chisq.test(data$variable, data$group)



########################################################################### END #############################################################################